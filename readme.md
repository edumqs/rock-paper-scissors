#Rock, Paper, Scissors

### DEMO? ###

-Yes! Test me on your phone :) **http://edupi2.noip.me:9000/rps**

### How do I get set up? ###

1. Clone this repository
2. **cd ../path-to-this-repo**
3. **npm install** (installs all the required dependencies)

### How to run the application? ###

1. **cd ../path-to-this-repo**
2. **npm run start**
3. Go to your browser and enter http://127.0.0.1:8080/ (make sure you don't have anything running on this port)

### How to run the tests? ###

1. **npm run watch-test**

User Story:
As a frequent games player, I'd like to play rock, paper, scissors so that I can spend an hour of my day having fun.

### Acceptance Criteria: ###                         	
  - Can I play Player vs Computer?