var React = require('react');

var Router = require('react-router').Router;
var Route = require('react-router').Route;
var IndexRoute = require('react-router').IndexRoute;
var browserHistory = require('react-router').browserHistory;

var render = require('react-dom').render;

var NoMatch = React.createClass({
    render: function(){
        return (
            <h2>Page not found</h2>
        );
    }
});

var routes = (
    <Route path="/" component={require('./js/components/Main.react')}>
        <IndexRoute component={require('./js/components/homepage/HomeScreen.react.js')}/>
        <Route path="play" component={require('./js/components/homepage/GameScreen.react.js')} />
        <Route path="*" component={NoMatch}/>
    </Route>
);

render(<Router history={browserHistory}>{routes}</Router>,document.getElementById('renderDiv'));

