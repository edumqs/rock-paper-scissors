var $ = require('jquery');

var rest = {
    put: function(url, data){
        return $.ajax({
            url: url,
            data: JSON.stringify(data),
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json'
            }
        });
    },
    post: function(url, data){
        return $.ajax({
            url: url,
            data: JSON.stringify(data),
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            }
        });
    },
    get: function(url, data){
        return $.ajax({
            dataType: 'json',
            url: url,
            data: JSON.stringify(data),
            method: 'GET'
        });
    },
    remove: function(url, data){
        return $.ajax({
            url: url,
            data: JSON.stringify(data),
            method: 'DELETE'
        });
    }
}

module.exports = rest;