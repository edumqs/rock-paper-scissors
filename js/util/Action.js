var Action = {
    Action: function(action, payload){
        return {
            actionType: action,
            payload: payload
        }
    }
};

module.exports = Action;
