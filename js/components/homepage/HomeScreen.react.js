var React = require('react');
var History = require('react-router').History;
var ReactDOM = require('react-dom');

var ReactBootstrap = require('react-bootstrap');
var Input = ReactBootstrap.Input;
var Row = ReactBootstrap.Row;
var Col = ReactBootstrap.Col;

var UserActions = require('../service/user/actions/UserActions');

var HomeScreen = React.createClass({
    mixins: [History],
    getInitialState: function(){
        return {
            isPlayerNameValid: undefined
        };
    },
    _play: function(){

        var playerName = this.refs.playerName.getValue();

        if(playerName === undefined || playerName == ""){
            this.setState({
                isPlayerNameValid: false
            });
            return;
        } else {
            UserActions.savePlayerName(playerName);
            this.history.push('/play');
        }

    },
    render: function(){
        return (
            <div className="home-screen">

                <Row>
                    <Col xs={12} className="diagram-img">
                        <img src="assets/diagram.png" alt="game diagram" />
                    </Col>
                </Row>

                <Row>
                    <Col xs={12} className="player-name-section">
                        <p>What's your name?</p>
                        <div className={this.state.isPlayerNameValid === false ? "warning-invalid" : ""}>
                            {this.state.isPlayerNameValid === false ? "*Please enter a name" : ""}
                            <Input id="playerName" ref="playerName" type="text" placeholder="Enter your name" />
                        </div>
                    </Col>
                </Row>

                <Row>
                    <Col xs={12} className="play">
                        <div onClick={this._play}><span>play</span></div>
                    </Col>
                </Row>

            </div>
        );
    }
});

module.exports = HomeScreen;