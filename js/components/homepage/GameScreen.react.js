var React = require('react');
var History = require('react-router').History;
var _ = require('underscore');

var ReactBootstrap = require('react-bootstrap');
var Row = require('react-bootstrap').Row;
var Col = require('react-bootstrap').Col;

var UserActions = require('../service/user/actions/UserActions');
var UserStore = require('../service/user/store/UserStore').UserStore;

var gameLogic = require('../game-cerebro/gameLogic');
var gameCerebro = require('../game-cerebro/cerebro');

var Weapons = gameLogic.weapons;
var roundResult = gameLogic.roundResult;

function capitalizeFirstLetter(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
}

var GameScreen = React.createClass({
    mixins: [History],
    getInitialState: function(){
        return {
            playerWeapon: undefined,
            computerWeapon: undefined
        };
    },
    componentWillMount: function(){
        if(UserStore.getPlayerName() === undefined){
            this.history.push('/');
        }
    },
    _onWeaponClick: function(weapon){

        var computerWeapon = gameCerebro.getWinPrediction();

        UserActions.storeUserPlay(weapon, computerWeapon);

        this.setState({
            playerWeapon: weapon,
            computerWeapon: computerWeapon
        });

    },
    _renderWeaponImage(weapon){
        switch(weapon){
            case 'paper': return <img src="assets/140 Paper.png"/>; break;
            case 'rock': return <img src="assets/140 Rock.png"/>; break;
            case 'scissors': return <img src="assets/140 Scissors.png"/>; break;
        }
    },
    _renderResult: function(){
        if(this.state.playerWeapon || this.state.computerWeapon){

            var result = roundResult(this.state.playerWeapon, this.state.computerWeapon);
            var message;

            if(result == "win"){
                message= "You won. Play again?";
            }else if(result == "lost"){
                message = "You lost. Play again?";
            }else{
                message = "You draw. Play again?";
            }

            return (
                <div className="weapons-result">
                    <h3 className="result-message">{message}</h3>
                    <Row>
                        <Col xs={5}>
                            <span>{this._renderWeaponImage(this.state.playerWeapon)}</span>
                        </Col>
                        <Col xs={2} className="result-forward-slash"><span>/</span></Col>
                        <Col xs={5}>
                            <span>{this._renderWeaponImage(this.state.computerWeapon)}</span>
                        </Col>
                    </Row>
                </div>
            );

        }

        return (
            <Row className="start-game-msg">
                <Col xs={12}>
                    <p>Choose your weapon!</p><p>Play now :)</p>
                </Col>
            </Row>
        );
    },
    _renderPlayerWeapons: function(){
        return (
            <Row className="player-weapons">
            {_.map(Weapons, function(weapon,index){
                return (
                    <Col key={"player_weapons_" + index} xs={4}>
                        <span onClick={this._onWeaponClick.bind(this, weapon)}>
                            <img src={"assets/60 " + capitalizeFirstLetter(weapon) + ".png"} alt={weapon + " weapon"} />
                        </span>
                    </Col>
                );
            }.bind(this))}
            </Row>
        );
    },
    render: function(){
        return (
            <div className="game-screen">

                {this._renderResult()}

                {this._renderPlayerWeapons()}

            </div>
        );
    }
});

module.exports = GameScreen;