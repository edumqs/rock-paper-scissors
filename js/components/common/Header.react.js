var React = require('react');

var Header = React.createClass({
    render: function(){

        return (
            <div className="header">
                <h2>Rock Paper Scissors</h2>
            </div>
        );
    }
});

module.exports = Header;