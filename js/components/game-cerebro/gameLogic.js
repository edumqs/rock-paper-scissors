var gameLogic = {
    weapons: ['paper', 'rock', 'scissors'],
    roundResult: function(player, computer){
        var result = ['win', 'lost', 'draw'];

        var combinations = {
            paper: {
                paper: 2,
                rock: 0,
                scissors: 1
            },
            rock: {
                paper: 1,
                rock: 2,
                scissors: 0
            },
            scissors: {
                paper: 0,
                rock: 1,
                scissors: 2
            }
        }

        return result[combinations[player][computer]];
    }

};

module.exports = gameLogic;