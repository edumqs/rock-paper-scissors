var expect = require('chai').expect;
var gameLogic = require('../gameLogic');

describe("gameLogic.spec", function(){

    describe("roundResult()", function(){

        it("should return win when the player plays paper and the computer plays rock", function(){

            expect(gameLogic.roundResult("paper", "rock")).to.equal('win');

        });

        it("should return draw when the player plays paper and the computer plays paper", function(){

            expect(gameLogic.roundResult("paper", "paper")).to.equal('draw');

        });

        it("should return lost when the player plays paper and the computer plays scissors", function(){

            expect(gameLogic.roundResult("paper", "scissors")).to.equal('lost');

        });

        it("should return lost when the player plays rock and the computer plays paper", function(){

            expect(gameLogic.roundResult("rock", "paper")).to.equal('lost');

        });

        it("should return draw when the player plays rock and the computer plays rock", function(){

            expect(gameLogic.roundResult("rock", "rock")).to.equal('draw');

        });

        it("should return win when the player plays rock and the computer plays scissors", function(){

            expect(gameLogic.roundResult("rock", "scissors")).to.equal('win');

        });

        it("should return win when the player plays scissors and the computer plays paper", function(){

            expect(gameLogic.roundResult("scissors", "paper")).to.equal('win');

        });

        it("should return lost when the player plays scissors and the computer plays rock", function(){

            expect(gameLogic.roundResult("scissors", "rock")).to.equal('lost');

        });

        it("should return draw when the player plays scissors and the computer plays scissors", function(){

            expect(gameLogic.roundResult("scissors", "scissors")).to.equal('draw');

        });

    });

});