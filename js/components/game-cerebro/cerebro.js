//Extra for this project :)
//Like Prof X the computer player will have its own CEREBRO to try to predict the player hand (or try :))

//Curiosity: Cerebro means Brain in Portuguese :)

var _ = require('underscore');
var DeepMemory = require('../service/user/store/UserStore').UserStore; //UserStore
var gameLogic = require('./gameLogic');

/*
    To predict the user's next hand we will use 2 basic indicators:
    1- most played hand
    2- most played hand after the hand played last.
    ex. if the player's last hand was paper we will check what is the hand that he usually plays after that
*/

var HandMappings = {
    paper: 0,
    rock: 1,
    scissors: 2
};

var mostPlayedHand = function(hands){
    var count = {
        paper: 0,
        rock: 0,
        scissors: 0
    };

    _.each(hands, function(hand){
        switch(hand.playerWeapon){
            case 0: count.paper = count.paper+1; break;
            case 1: count.rock = count.rock+1; break;
            case 2: count.scissors = count.scissors+1; break;
        }
    });

    var max = {
        name: "",
        count: 0
    };

    for (var property in count) {
        if (count.hasOwnProperty(property)) {
            if(count[property] > max.count){
                max.name = property;
                max.count = count[property];
            }
        }
    }

    return max;
}

var mostPlayedHandAfterLast = function(hands){

    if(hands === undefined)
        return undefined;

    var lastHand = hands[hands.length-1];

    var newHands = [];

    for(var i=0; i<hands.length; i++){

        if(hands[i].playerWeapon == lastHand.playerWeapon && (i+1 <= hands.length) && i+1 != hands.length){
            newHands.push(hands[i+1]);
        }
    }

    if(newHands.length == 0)
        return undefined;

    return mostPlayedHand(newHands);

}

var gameCerebro = {
    getWinPrediction: function(){

        var deepMemoryHistory = DeepMemory.getHandHistory();

        //if there is no data in memory we will just send a random hand
        if(deepMemoryHistory === undefined)
            return gameLogic.weapons[(Math.floor((Math.random() * 3) + 1))-1];

        var mph = mostPlayedHand(deepMemoryHistory);
        var mphAfterLast = mostPlayedHandAfterLast(deepMemoryHistory);

        if((mph && mphAfterLast) && mph.name == mphAfterLast.name){

            var handIndex = HandMappings[mph.name];

            if(handIndex > 0) {
                handIndex--;
            }else{
                handIndex = 2;
            }

            return gameLogic.weapons[handIndex];
        }

        return gameLogic.weapons[(Math.floor((Math.random() * 3) + 1))-1];

    }
};


module.exports = gameCerebro;