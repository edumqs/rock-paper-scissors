var Dispatcher = require('../../dispatcher/Dispatcher');
var Action = require('../../../../util/Action').Action;
//var Rest = require('../../../../util/rest');
//var Q = require('q');

var Constants = require('../Constants');

var UserActions = {
    savePlayerName: function(playerName){
        Dispatcher.dispatch(new Action(Constants.STORE_PLAYER_NAME, playerName));
    },
    storeUserPlay: function(weapon, computerWeapon){
        Dispatcher.dispatch(new Action(Constants.STORE_LAST_ROUND, {
            playerWeapon: weapon,
            computerWeapon: computerWeapon
        }));
    }
};

module.exports = UserActions;
