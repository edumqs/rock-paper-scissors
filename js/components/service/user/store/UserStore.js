var Dispatcher = require('../../dispatcher/Dispatcher');
var EventEmitter = require('events').EventEmitter;
var Constants = require('../Constants');
var assign = require('object-assign');
var _ = require('underscore');

var CHANGE_EVENT = 'change';

var _state = undefined;

var getState =  function (){
    _state =  _state || {};
    return _state;
};

var HandMappings = {
    paper: 0,
    rock: 1,
    scissors: 2
};

var UserStore = assign({}, EventEmitter.prototype, {
    getPlayerName: function(){
        return getState().playerName;
    },
    getHandHistory: function(){
        return getState().playerHands;
    },
    emitChange: function() {
        this.emit(CHANGE_EVENT);
    },
    addChangeListener: function(callback) {
        this.on(CHANGE_EVENT, callback);
    },
    removeChangeListener: function(callback) {
        this.removeListener(CHANGE_EVENT, callback);
    }
});

// Register callback to handle all updates
Dispatcher.register(function(action) {

    switch(action.actionType) {

        case Constants.STORE_PLAYER_NAME:

            getState().playerName = action.payload;

            UserStore.emitChange(); // just the components listening to this store will receive updates
            break;
        case Constants.STORE_LAST_ROUND:

            if(getState().playerHands === undefined)
                getState().playerHands = [];

            var hand = {
                playerWeapon: HandMappings[action.payload.playerWeapon],
                computerWeapon: HandMappings[action.payload.computerWeapon]
            };

            getState().playerHands.push(hand);

            UserStore.emitChange(); // just the components listening to this store will receive updates
            break;
        default:
    }
});

module.exports = {
    UserStore: UserStore
};