var expect = require('chai').expect;

var Constants = require('../../Constants');
var Dispatcher = require('../../../dispatcher/Dispatcher');
var Action = require('../../../../../util/Action').Action;
var UserStoreModule = require('../UserStore');
var UserStore = UserStoreModule.UserStore;

function clearStoreState () {
    UserStoreModule.__set__({
        _state: undefined
    });
}

beforeEach(clearStoreState);

//these tests do not provide full coverage of the application, it's just a example
describe("UserStore.spec", function(){

    describe("getPlayerName()", function(){

        it("should return undefined if the username has not been stored", function(){

            expect(UserStore.getPlayerName()).to.be.undefined;

        });

        it("should return the player name saved in the store", function(){

            Dispatcher.dispatch(new Action(Constants.STORE_PLAYER_NAME, "random_user_name"));

            expect(UserStore.getPlayerName()).to.equal("random_user_name");

        });

    });

    describe("getHandHistory()", function(){
        it("should return undefined if there is no data in the store", function(){

            expect(UserStore.getHandHistory()).to.be.undefined;

        });

        it("should return the hands history saved in the store", function(){

            var hand1 = {
                playerWeapon: "paper",
                computerWeapon: "rock"
            };

            var hand2 = {
                playerWeapon: "rock",
                computerWeapon: "scissors"
            };

            Dispatcher.dispatch(new Action(Constants.STORE_LAST_ROUND, hand1));
            Dispatcher.dispatch(new Action(Constants.STORE_LAST_ROUND, hand2));

            expect(UserStore.getHandHistory()).to.have.length(2);
            expect(UserStore.getHandHistory()).to.deep.equal([{
                playerWeapon: 0,
                computerWeapon: 1
            },{
                playerWeapon: 1,
                computerWeapon: 2
            }]);
        });

    });

});