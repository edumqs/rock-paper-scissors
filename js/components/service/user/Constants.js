var keymirror = require('keymirror');

module.exports = {
    STORE_PLAYER_NAME: null,
    STORE_LAST_ROUND: null
};

module.exports = (keymirror(module.exports));